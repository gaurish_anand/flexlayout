import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TextPropTypes,
} from "react-native";
import { LinearLayout } from "./Main/LinearLayout";
import { LinearLayoutItem } from "./Main/LinearLayoutItem";

const ExampleOne = (props) => {
  const Widget1 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "yellow",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 1</Text>
    </View>
  );
  const Widget2 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "green",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 2</Text>
    </View>
  );
  const Widget3 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#ffffcc",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 3</Text>
    </View>
  );
  const Widget4 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "orange",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 4</Text>
    </View>
  );
  const Widget5 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#9999ff",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 5</Text>
    </View>
  );
  const Widget6 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#9900cc",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 6</Text>
    </View>
  );
  const Widget7 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#cccc00",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 7</Text>
    </View>
  );
  const Widget8 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#669999",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 8</Text>
    </View>
  );
  const Widget9 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#cc3300",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 9</Text>
    </View>
  );
  const Banner1 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#66ff99",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Banner 1</Text>
    </View>
  );
  const Banner2 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#9900cc",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Banner 2</Text>
    </View>
  );

  return (
    <LinearLayout flexDirection="column" style={{ backgroundColor: "#ffcccc" }}>
      <LinearLayoutItem span="1" flex="12">
        <Banner1 />
      </LinearLayoutItem>
      <LinearLayoutItem span="9" flex="12">
        <LinearLayout flexDirection="row" flexWrap="wrap">
          <LinearLayoutItem span="4" flex="4">
            <Widget1 />
          </LinearLayoutItem>
          <LinearLayoutItem span="4" flex="4">
            <Widget2 />
          </LinearLayoutItem>
          <LinearLayoutItem span="4" flex="4">
            <Widget3 />
          </LinearLayoutItem>
          <LinearLayoutItem span="4" flex="4">
            <Widget4 />
          </LinearLayoutItem>
          <LinearLayoutItem span="4" flex="4">
            <Widget5 />
          </LinearLayoutItem>
          <LinearLayoutItem span="4" flex="4">
            <Widget6 />
          </LinearLayoutItem>
          <LinearLayoutItem span="4" flex="4">
            <Widget7 />
          </LinearLayoutItem>
          <LinearLayoutItem span="4" flex="4">
            <Widget8 />
          </LinearLayoutItem>
          <LinearLayoutItem span="4" flex="4">
            <Widget9 />
          </LinearLayoutItem>
        </LinearLayout>
      </LinearLayoutItem>
      <LinearLayoutItem span="2" flex="12">
        <Banner2 />
      </LinearLayoutItem>
    </LinearLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    marginLeft: 10,
    marginRight: 10,
    width: "90%",
    borderWidth: 5,
    borderColor: "red",
    paddingHorizontal: 5,
    paddingVertical: 10,
  },
  default: {
    // borderRadius: 10,
    margin: 10,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default ExampleOne;
