import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { LinearLayoutItem } from "./Main/LinearLayoutItem";
import { LinearLayout } from "./Main/LinearLayout";
import MyListView from "./MyListView";

const ExampleFour = (props) => {
  const ScrollView = (props) => (
    <View style={{ backgroundColor: "#DAE1DF", flex: 1 }}>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Saved Filters</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Size</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Color</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Brand</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Categories</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Bundles</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Country of Origin</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>More Filters</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Saved Filters</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Price Range</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Discount</Text>
      </View>
      <View
        style={{
          width: "100%",
          height: 53,
          borderBottomWidth: 0.2,
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <Text style={{ padding: 6, fontSize: 15 }}>Delivery Time</Text>
      </View>
    </View>
  );
  const Widget1 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#C6DE9E",
      }}
    >
      <Text>Widget 1</Text>
    </View>
  );
  const Widget2 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#cc6699",
      }}
    >
      <Text>Widget 2</Text>
    </View>
  );

  return (
    <LinearLayout flexDirection="row">
      <LinearLayoutItem span="3">
        <ScrollView />
      </LinearLayoutItem>
      <LinearLayoutItem span="9">
        <LinearLayout flexDirection="column">
          <LinearLayoutItem span="1">
            <Widget1 />
          </LinearLayoutItem>
          <LinearLayoutItem span="5" isDynamic="true">
            <MyListView quantity="5" />
          </LinearLayoutItem>
          <LinearLayoutItem span="5" isDynamic="true">
            <MyListView quantity="5" />
          </LinearLayoutItem>
          <LinearLayoutItem>
            <Widget2 />
          </LinearLayoutItem>
        </LinearLayout>
      </LinearLayoutItem>
    </LinearLayout>
  );
};
const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
  },
  default: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
});

export default ExampleFour;
