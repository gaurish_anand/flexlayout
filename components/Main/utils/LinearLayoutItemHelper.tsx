import { LinearLayoutItemProps } from "./LayoutProps";
import React from "react";

export type Dimensions = {
  height: Number | string;
  width: Number | string;
};

/**
 * Here span is always along the width and flex is always along the height.
 */
const getChildDimensions = function (
  span: String | undefined,
  flex: String | undefined
): Dimensions {
  const childHeight = (Number(flex) * 100) / 12 + "%";
  const childWidth = (Number(span) * 100) / 12 + "%";
  let dimension: Dimensions = {
    height: childHeight,
    width: childWidth,
  };
  return dimension;
};

export function getStyles(
  span: String | undefined,
  flex: String | undefined,
  isDynamic: String,
  style: any,
  parentFlexDirection: String
): any {
  const { height, width } =
    parentFlexDirection === "column"
      ? getChildDimensions(flex, span)
      : getChildDimensions(span, flex);

  if (isDynamic === "true") {
    if (parentFlexDirection === "column") {
      return [
        {
          flexGrow: 0,
          width: width,
          minHeight: 1,
          maxHeight: height,
        },
        style,
      ];
    } else {
      return [
        {
          flexGrow: 0,
          height: height,
          minWidth: 1,
          maxWidth: width,
        },
        style,
      ];
    }
  } else {
    return [
      {
        width: width,
        height: height,
      },
      style,
    ];
  }
}

const getUndefinedSpanFlex = function (children: React.ReactNode) {
  let leftOverSpan = 12;
  let totalSpanUndefined = 0;

  React.Children.toArray(children).map((child) => {
    if (!React.isValidElement<LinearLayoutItemProps>(child)) {
      return child;
    }
    const props = { ...child.props };
    if (props.span === undefined) {
      totalSpanUndefined++;
    } else {
      leftOverSpan -= Number(props.span);
    }
  });

  return {
    leftOverSpan,
    totalSpanUndefined,
  };
};

export function getChildren(
  children: React.ReactNode,
  flexDirection: "row" | "column"
) {
  /**
   * Here first update the span if not mentioned equally so that everyone takes equal space.
   */
  const { leftOverSpan, totalSpanUndefined } = getUndefinedSpanFlex(children);

  return React.Children.toArray(children).map((child) => {
    if (!React.isValidElement<LinearLayoutItemProps>(child)) {
      return child;
    }
    const updatesProps = { ...child.props };
    // set new props here on newProps
    updatesProps.parentFlexDirection = flexDirection;

    // Dividing span and flex equally if not defined equally.
    if (updatesProps.span === undefined) {
      updatesProps.span = String(leftOverSpan / totalSpanUndefined);
    }
    if (updatesProps.flex === undefined) {
      updatesProps.flex = "12";
    }
    const preparedChild = { ...child, props: updatesProps };
    return preparedChild;
  });
}
