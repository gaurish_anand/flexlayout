import React from "react";

/**
 * flexDirection - It will be used to mention the direction in which all the LinearLayoutItems inside will be layed out. By default if
 *                 it is not mentioned then it will be column only.
 * ------------------------------------------------------------------------------
 * flexWrap - By default this layout will be "nowrap". But if we want that after not able to render on the same row / column
 *            it should move to the next row / column. Then we should use this flexWrap = "wrap".
 * ------------------------------------------------------------------------------
 * style - For each LinearLayoutItem we can give styling such as margin , padding, border , selfAlign , etc.
 * ------------------------------------------------------------------------------
 * alignItems: "flex-start" | "flex-end" | "center" | "baseline" | "stretch"
 * By default all the items will be aligned on the left in vertical Layout and top in Horizontal Layout.
 * But if we want to change the alignment, then we can use alignItems to arrange them inside this Layout.
 */
export type LinearLayoutProps = {
  flexDirection: "column" | "row";
  flexWrap: "nowrap" | "wrap";
  style?: any;
  alignItems: "flex-start" | "flex-end" | "center" | "baseline" | "stretch";
  children?: React.ReactNode;
};

/**
 * span and flex - is the ratio of the length across the main and cross axis respectively.
 *                 If span is undefined, Then the length along the main axis will be equally divided among children. *
 *                 If flex is undefined, Then the default value will be 12 i.e full length along the cross axis.
 * ------------------------------------------------------------------------------
 * isDynamic - It will help us in maintaing the maximum length of the widget along the main axis. We can basically use it
 *             in case of List if we are not having enough items to display then it will automatically collapse on the Main Axis.
 * ------------------------------------------------------------------------------
 * style - For each LinearLayoutItem we can give styling such as margin , padding, border , selfAlign , etc.
 */
export type LinearLayoutItemProps = {
  span: String | undefined;
  flex: String | undefined;
  isDynamic: "true" | "false";
  style?: any;
  children?: JSX.Element | JSX.Element[] | null;
  parentFlexDirection: "row" | "column";
};
