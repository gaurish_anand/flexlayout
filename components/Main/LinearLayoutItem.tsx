import React, { FunctionComponent } from "react";
import { View } from "react-native";
import { LinearLayoutItemProps } from "./utils/LayoutProps";
import { getStyles } from "./utils/LinearLayoutItemHelper";

/**
 * span - it is the ratio of the length of the widget on main axis. By default if not mentioned then all the items will be equally divided with left over space for those
 *        whose span is not mentioned
 * flex - it is the ratio of the length of the widget on cross axis. By default Item will take full length if not mentioned, item will take full length across cross axis.
 * isDynamic - It will help us in maintaing the maximum length of the widget along the main axis. Example - We can use it
 *             in case of List if we are not having enough items to display then it will automatically collapse on the Main Axis.
 * style - We can also mention styles for each LinearLayoutItem (e.g - margin , padding , content alignment,etc)
 */
export const LinearLayoutItem: FunctionComponent<LinearLayoutItemProps> = ({
  span = undefined,
  flex = "12",
  isDynamic = "false",
  style,
  parentFlexDirection = "column",
  children,
}) => {
  return (
    <View style={getStyles(span, flex, isDynamic, style, parentFlexDirection)}>
      {children}
    </View>
  );
};
