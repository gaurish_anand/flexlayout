import React from "react";
import { StyleSheet, View } from "react-native";
import { LinearLayoutProps } from "./utils/LayoutProps";
import { getChildren } from "./utils/LinearLayoutItemHelper";

/**
 * flexDirection: "column" | "row"
 * By default all the children will be arranged vertically i.e flexDirection = "column"
 *
 * ------------------------------------------------------------------------------
 *
 * alignItems: "flex-start" | "flex-end" | "center" | "baseline" | "stretch"
 * By default all the items will be aligned on the left in vertical Layout and top in Horizontal Layout.
 * But if we want to change the alignment, then we can use alignItems to arrange them inside this Layout.
 *
 * ------------------------------------------------------------------------------
 *
 * flexWrap: "nowrap" | "wrap"
 * By default Layout will be nowrap.
 *
 *
 * ------------------------------------------------------------------------------
 *
 * styles -
 * By default the Layout will take full height and width , but if we want we can mention the required height , width in styles
 *
 * ------------------------------------------------------------------------------
 *
 */
export const LinearLayout: React.FC<LinearLayoutProps> = ({
  flexDirection = "column",
  alignItems = "flex-start",
  flexWrap = "nowrap",
  style,
  children,
}) => {
  return (
    <View
      style={[
        styles.container,
        style,
        {
          flexDirection,
          alignItems,
          flexWrap,
        },
      ]}
    >
      {getChildren(children, flexDirection)}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
  },
});
