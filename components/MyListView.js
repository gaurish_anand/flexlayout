import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  Image,
  FlatList,
  ScrollView,
} from "react-native";
import {
  RecyclerListView,
  DataProvider,
  LayoutProvider,
} from "recyclerlistview";

const SCREEN_WIDTH = Dimensions.get("window").width;

export default class MyListView extends Component {
  constructor(props) {
    super(props);

    const fakeData = [];
    for (let i = 1; i <= props.quantity; i += 1) {
      fakeData.push({
        type: "NORMAL",
        id: i,
        item: `Widget ${i}`,
      });
    }
    this.state = {
      list: new DataProvider((r1, r2) => r1 !== r2).cloneWithRows(fakeData),
      quantity: props.quantity,
      fakeData: fakeData,
    };

    this.layoutProvider = new LayoutProvider(
      (i) => {
        return i;
      },
      (type, dim) => {
        dim.width = SCREEN_WIDTH;
        dim.height = 50;
      }
    );
  }

  rowRenderer = (type, data) => {
    return (
      <View
        style={{
          height: "100%",
          width: "100%",
          borderWidth: 2,
          borderColor: "red",
        }}
      >
        <Text style={{ fontSize: 20, fontWeight: "bold" }}>{data.item}</Text>
      </View>
    );
  };

  renderItemComponent = (data) => (
    <View
      style={{
        height: "100%",
        width: "100%",
        borderWidth: 2,
        borderColor: "red",
      }}
    >
      <Text style={{ fontSize: 20, fontWeight: "bold" }}>{item}</Text>
    </View>
  );

  render() {
    return (
      <FlatList
        // style={{ width: 200 }}//% won't work if i don't define the height.
        data={this.state.fakeData}
        keyExtractor={(item, index) => item + index.toString()}
        renderItem={({ item }) => (
          <View
            style={{
              height: 50,
              width: "100%",
              borderWidth: 0.5,
              justifyContent: "center",
              paddingLeft: 15,
            }}
          >
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>
              {item.item}
            </Text>
          </View>
        )}
      />
      // <RecyclerListView
      //   rowRenderer={this.rowRenderer}
      //   dataProvider={this.state.list}
      //   layoutProvider={this.layoutProvider}
      // />

      // <View
      //   flexDirection="col"
      //   style={{
      //     height: "100%",
      //     width: "100%",
      //     borderWidth: 5,
      //     borderColor: "brown",
      //   }}
      // >
      //   <View
      //     style={{ height: "10%", width: "100%", backgroundColor: "orange" }}
      //   ></View>
      //   <View style={{ flexGrow: 0, minHeight: 1, maxHeight: 800 }}>
      //     <RecyclerListView
      //       rowRenderer={this.rowRenderer}
      //       dataProvider={this.state.list}
      //       layoutProvider={this.layoutProvider}
      //     />
      //   </View>
      //   <View
      //     style={{ height: "10%", width: "100%", backgroundColor: "orange" }}
      //   ></View>
      // </View>
    );
  }
}

{
  /*  */
}
