import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TextPropTypes,
} from "react-native";
import { LinearLayout } from "./Main/LinearLayout";
import { LinearLayoutItem } from "./Main/LinearLayoutItem";

const ExampleTwo = (props) => {
  const Widget1 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "yellow",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 1</Text>
    </View>
  );
  const Widget2 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "green",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 2</Text>
    </View>
  );
  const Widget3 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#ffffcc",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 3</Text>
    </View>
  );
  const Widget4 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "orange",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 4</Text>
    </View>
  );
  const Widget5 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#9999ff",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 5</Text>
    </View>
  );
  const Widget6 = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#9900cc",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>Widget 6</Text>
    </View>
  );
  return (
    <LinearLayout
      flexDirection="column"
      alignItems="center"
      style={{ backgroundColor: "#ffcccc" }}
      isScrollable="true"
    >
      <LinearLayoutItem span="1">
        <Text
          style={{
            fontWeight: "bold",
            marginTop: 20,
            alignSelf: "center",
          }}
        >
          FLY-OFF-THE-SHELF STYLES
        </Text>
        <Text style={{ marginBottom: 20, alignSelf: "center" }}>
          Blink, And They're Gone!
        </Text>
      </LinearLayoutItem>
      <LinearLayoutItem flex="12">
        <LinearLayout flexDirection="row">
          <LinearLayoutItem flex="12">
            <Widget1 />
          </LinearLayoutItem>
          <LinearLayoutItem flex="12">
            <Widget2 />
          </LinearLayoutItem>
        </LinearLayout>
      </LinearLayoutItem>
      <LinearLayoutItem flex="12">
        <LinearLayout flexDirection="row">
          <LinearLayoutItem span="6" flex="12">
            <Widget3 />
          </LinearLayoutItem>
          <LinearLayoutItem span="6" flex="12">
            <Widget4 />
          </LinearLayoutItem>
        </LinearLayout>
      </LinearLayoutItem>
    </LinearLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    marginLeft: 10,
    marginRight: 10,
    width: "90%",
    borderWidth: 5,
    borderColor: "red",
    paddingHorizontal: 5,
    paddingVertical: 10,
  },
  default: {
    // borderRadius: 10,
    margin: 10,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default ExampleTwo;
