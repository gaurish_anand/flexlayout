import React from "react";
import { StyleSheet, Text, View, FlatList, Dimensions } from "react-native";
import ExampleFour from "./ExampleFour";
import ExampleOne from "./ExampleOne";
import ExampleThree from "./ExampleThree";
import ExampleTwo from "./ExampleTwo";
import { LinearLayout } from "./Main/LinearLayout";
import { LinearLayoutItem } from "./Main/LinearLayoutItem";

const ExampleFive = (props) => {
  const getViews = function (index) {
    const number = index % 4;
    if (number === 0) {
      return (
        <View style={{ height: 835, width: "100%" }}>
          <ExampleOne />
        </View>
      );
    } else if (number === 1) {
      return (
        <View style={{ height: 835, width: "100%" }}>
          <ExampleTwo />
        </View>
      );
    } else if (number === 2) {
      return (
        <View style={{ height: 835, width: "100%" }}>
          <ExampleThree />
        </View>
      );
    } else {
      return (
        <View style={{ height: 835, width: "100%" }}>
          <ExampleFour />
        </View>
      );
    }
  };

  return (
    <FlatList
      data={Array.from(Array(100).keys())}
      keyExtractor={(item, index) => item + index.toString()}
      renderItem={({ item, index }) => getViews(index)}
    />
  );
};

const styles = StyleSheet.create({});

export default ExampleFive;
