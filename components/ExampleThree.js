import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { LinearLayoutItem } from "./Main/LinearLayoutItem";
import { LinearLayout } from "./Main/LinearLayout";

const ExampleThree = (props) => {
  const SlideShow = (props) => (
    <View
      style={{
        ...styles.default,
        backgroundColor: "#669999",
      }}
    >
      <Text style={{ fontWeight: "bold", fontSize: 40 }}>Slide Show</Text>
    </View>
  );
  const ProductName = (props) => (
    <Text style={{ fontWeight: "bold", fontSize: 20 }}>Product Name</Text>
  );
  const OriginalPrice = (props) => (
    <Text
      style={{
        fontWeight: "bold",
        textDecorationLine: "line-through",
        textDecorationStyle: "solid",
      }}
    >
      Original Price
    </Text>
  );
  const DiscountedPrice = (props) => <Text>Discounted Price</Text>;
  const TaxComponent = (props) => (
    <Text
      style={{
        color: "#006600",
        fontWeight: "bold",
      }}
    >
      inclusive of all taxes
    </Text>
  );
  const WishListButton = (props) => (
    <View
      style={{
        ...styles.default,
        borderRadius: 8,
        borderWidth: 0.2,
        backgroundColor: "white",
      }}
    >
      <Text style={{ fontWeight: "bold" }}>WISHLIST</Text>
    </View>
  );
  const AddToBagButton = (props) => (
    <View
      style={{
        ...styles.default,
        borderRadius: 8,
        backgroundColor: "#ff3300",
      }}
    >
      <Text style={{ fontWeight: "bold", color: "white" }}>ADD TO BAG</Text>
    </View>
  );
  return (
    <LinearLayout>
      <LinearLayoutItem span="10">
        <SlideShow />
      </LinearLayoutItem>
      <LinearLayoutItem span="0.5" style={{ padding: 8 }}>
        <ProductName />
      </LinearLayoutItem>
      <LinearLayoutItem span="0.5" style={{ padding: 8 }}>
        <LinearLayout flexDirection="row">
          <LinearLayoutItem span="3.4">
            <DiscountedPrice />
          </LinearLayoutItem>
          <LinearLayoutItem span="3">
            <OriginalPrice />
          </LinearLayoutItem>
        </LinearLayout>
      </LinearLayoutItem>
      <LinearLayoutItem span="0.3" style={{ paddingLeft: 8 }}>
        <TaxComponent />
      </LinearLayoutItem>
      <LinearLayoutItem span="0.6" style={{ paddingHorizontal: 8 }}>
        <LinearLayout flexDirection="row" alignItems="center">
          <LinearLayoutItem span="4">
            <WishListButton />
          </LinearLayoutItem>
          <LinearLayoutItem span="7.8" style={{ marginLeft: 5 }}>
            <AddToBagButton />
          </LinearLayoutItem>
        </LinearLayout>
      </LinearLayoutItem>
    </LinearLayout>
  );
};

const styles = StyleSheet.create({
  default: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default ExampleThree;

{
  /* <View
          span="8"
          style={{
            ...styles.default,
            backgroundColor: "green",
            height: 200,
          }}
        >
          <Text>Widget 10</Text>
        </View> */
}
