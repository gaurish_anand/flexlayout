import React from "react";
import { SafeAreaView } from "react-native";
import ExampleOne from "./components/ExampleOne";
import ExampleTwo from "./components/ExampleTwo";
import ExampleThree from "./components/ExampleThree";
import ExampleFour from "./components/ExampleFour";
import ExampleFive from "./components/ExampleFive";

export default function App() {
  return (
    <SafeAreaView>
      {/* <ExampleOne /> */}
      {/* <ExampleTwo /> */}
      {/* <ExampleThree /> */}
      <ExampleFour />
      {/* <ExampleFive /> */}
    </SafeAreaView>
  );
}
